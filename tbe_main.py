#!/usr/bin/env python3

# Ashutosh Chaudhari
# Intern
# OEC Mechanical

"""
    Module to extract specific row with the given index, for QoL
"""

import pandas as pd


class TBE:

    def __init__(self, location):
        self.location = location
        self.parse_file()
        

    def parse_file(self):
        rename_dict = {
            "BIDDER-1": "BIDDER-1- C/ NC",
            "BIDDER-2": "BIDDER-2- C/ NC",
            "BIDDER-3": "BIDDER-3- C/ NC",
            "Unnamed: 2":"SUB-PARAM",
            "Unnamed: 9":"A/ NA",
            "Unnamed: 10":"Remarks",
            "Unnamed: 13":"A/ NA",
            "Unnamed: 14":"Remarks",
            "Unnamed: 18":"A/NA",
            "Unnamed: 19":"Remarks",
        }
        self.df = pd.read_excel(f"{self.location}", header=7, usecols="A:V", engine='openpyxl')
        # self.df = self.df[[f'Unnamed: {str(i)}' for i in range(26)]]
        # self.df = self.df.drop(['PARAMETERS','Unnamed: 3','Unnamed: 4','Unnamed: 5','Unnamed: 7', 'Unnamed: 11','Unnamed: 15','Unnamed: 16','Unnamed: 20'], axis=1) 
        self.df = self.df.dropna(axis=1, how='all')
        self.df = self.df.drop(['PARAMETERS','Unnamed: 3','Unnamed: 4'], axis=1)
        self.df = self.df.drop(range(0,6), axis=0)                              # Deletes some non useful rows from the dataframe to make simple
        self.df.rename(columns=rename_dict,
                                    inplace=True)
        self.df.head()
        

    def show(self):
        print(self.df)

    def get_param(self, index):
        self.index = index
        self.result=self.df[self.df[f'Line No.']==self.index]
        # print(self.df[self.df])
        return (self.result)

    def keyword_search(self, keyword1):
        # might need to use loc and contains()
        self.keyword1 = keyword1
        self.new_search_df = self.df.loc[(self.df['SUB-PARAM']).str.contains(self.keyword1, case=False, na=False)]
        return (self.new_search_df)

    def search_df(self, keyword):
        self.keyword = keyword
        pass

    def df_to_excel(self):
        # this method is for debugging
        self.new_excel_loc = 'eh_new_excel.xlsx'   
        self.df.to_excel(self.new_excel_loc)
